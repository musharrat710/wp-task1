<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'WR>a]#85?HPbcslYg]&EA}o^& RoC%V2;D!D!HMfOgV,Z]2X$9(m`6v#_*|]lMVd' );
define( 'SECURE_AUTH_KEY',  '4iR R-!!@5u.,S65Hz3X<<hN3i.5O~Sdt$*G0 (Y{h3_BU1x=tY[zL9AF?jhvf#U' );
define( 'LOGGED_IN_KEY',    '#D9{&CLX[j4Kw0:0KAJWKyJv!_6lw;|?<D%Vg}%QO7VV]Z1FMn=b|T509d$2[K?J' );
define( 'NONCE_KEY',        'x9r||sTQyO79NjNd>G|}/gH;6jn0)KiJA&8%VFfg2O<HBGIf*)!iD@~=EnK9{+<p' );
define( 'AUTH_SALT',        'Pu0{&!4Sz|?!k0oHI4RWg4k#5+y8/~g`Q^:[e X_IVSIhuM5&YCk!Nt5R~WQ[hay' );
define( 'SECURE_AUTH_SALT', '6_FEfSo8oC?bkAv2XqDc_.nY<.j+$z/Eqx)DwJ^}F_vL9rlj!LfuNB.X7;aQX<as' );
define( 'LOGGED_IN_SALT',   'mWQf[PA)H;6YIJ.JGRS1Ol5mouN<Ipw]8`kHzYO8IA5+_A3fhP&x}RcO 3x||TXS' );
define( 'NONCE_SALT',       'A<m00atn]J/?O=GgMecqrAr4/<q6J;j5FKkG^+mo6eZhs>K&iHt|5BS6 gE`:4RJ' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
