<?php
/*
 Template Name: Book Registration Form
 */

get_header();

<form role='form' method="post">
  <div class='form-group'>
    <input id='title' name='title' type='text' placeholder='Book Title'
	class='form-control input-sm' required=''>
  </div>
  <div class='form-group'>
       <input id='author' name='author' type='text' placeholder='Author Name'
	   class='form-control input-sm' required=''>
  </div>
  <div class='row justify-content-center'>
    <div class='col-xs-4 col-sm-4 col-md-4'>
	   <input type='submit' value='Submit'
	   class='btn btn-info btn-block' name='submitbtn'>
	</div>    
  </div>
</form> 

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	get_template_part( 'template-parts/content/content-page' );

	// If comments are open or there is at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
endwhile; // End of the loop.

get_footer();
